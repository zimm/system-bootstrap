#!/bin/bash

yum clean all                          && \
rm -rf /var/cache/yum                  && \
yum update -y                          && \
package-cleanup --oldkernels --count=2 && \
grub2-mkconfig -o /boot/grub2/grub.cfg
