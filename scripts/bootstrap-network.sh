#!/bin/bash

##############################################################################
# Configuration Variables
#
NETWORK_FILE=/etc/sysconfig/network
ETH0_FILE=/etc/sysconfig/network-scripts/ifcfg-eth0
HOSTS_FILE=/etc/hosts

##############################################################################
# Sanity Checks and Confirmations
#
if [ "$(id -u)" != "0" ]; then
    echo "This must be run as root" 1>&2
    exit 1
fi

echo ' '
echo 'Network Config Bootstrap'
echo ' '
echo 'Please note there is no error checking and this server will reboot upon completion.'
echo ' '
echo '!ONLY RUN THIS SCRIPT ON NEW MACHINES!'
echo ' '
read -p "Press any key to continue."

##############################################################################
# Collect Values
#
OLD_HOSTNAME=`hostname`
echo "Enter HOSTNAME [$OLD_HOSTNAME]:"
read NEW_HOSTNAME
if [ -z "$NEW_HOSTNAME" ]; then
    NEW_HOSTNAME=$OLD_HOSTNAME
fi

OLD_DOMAIN=`cat $ETH0_FILE | grep DOMAIN | awk -F '=' '{print $2}'`
echo "Enter DOMAIN [$OLD_DOMAIN]:"
read NEW_DOMAIN
if [ -z "$NEW_DOMAIN" ]; then
    NEW_DOMAIN=$OLD_DOMAIN
fi

OLD_IPADDR=`cat $ETH0_FILE | grep IPADDR | awk -F '=' '{print $2}'`
echo "Enter IPADDR [$OLD_IPADDR]:"
read NEW_IPADDR
if [ -z "$NEW_IPADDR" ]; then
    NEW_IPADDR=$OLD_IPADDR
fi

OLD_NETMASK=`cat $ETH0_FILE | grep NETMASK | awk -F '=' '{print $2}'`
echo "Enter NETMASK [$OLD_NETMASK]:"
read NEW_NETMASK
if [ -z "$NEW_NETMASK" ]; then
    NEW_NETMASK=$OLD_NETMASK
fi

OLD_GATEWAY=`cat $ETH0_FILE | grep GATEWAY | awk -F '=' '{print $2}'`
echo "Enter GATEWAY [$OLD_GATEWAY]:"
read NEW_GATEWAY
if [ -z "$NEW_GATEWAY" ]; then
    NEW_GATEWAY=$OLD_GATEWAY
fi

OLD_DNS1=`cat $ETH0_FILE | grep DNS1 | awk -F '=' '{print $2}'`
echo "Enter DNS1 [$OLD_DNS1]:"
read NEW_DNS1
if [ -z "$NEW_DNS1" ]; then
    NEW_DNS1=$OLD_DNS1
fi

OLD_DNS2=`cat $ETH0_FILE | grep DNS2 | awk -F '=' '{print $2}'`
echo "Enter DNS2 [$OLD_DNS2]:"
read NEW_DNS2
if [ -z "$NEW_DNS2" ]; then
    NEW_DNS2=$OLD_DNS2
fi

##############################################################################
# Confirm Values
#
echo "The following changes will be made:"
echo "  HOSTNAME: $NEW_HOSTNAME (was $OLD_HOSTNAME)"
echo "    DOMAIN: $NEW_DOMAIN (was $OLD_DOMAIN)"
echo "    IPADDR: $NEW_IPADDR (was $OLD_IPADDR)"
echo "   NETMASK: $NEW_NETMASK (was $OLD_NETMASK)"
echo "   GATEWAY: $NEW_GATEWAY (was $OLD_GATEWAY)"
echo "      DNS1: $NEW_DNS1 (was $OLD_DNS1)"
echo "      DNS2: $NEW_DNS2 (was $OLD_DNS2)"
echo " "
echo "      NOTE: You will still need to install the puppet agent after reboot."
while true; do
    read -p "Regenerate ssh keys, apply changes and reboot? [y/n]: " YN
    case $YN in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "You must answer yes or no.";;
    esac
done

##############################################################################
# Set Values and Generalize
#

# Set HOSTNAME
sed -i "s/^HOSTNAME.*$/HOSTNAME=$NEW_HOSTNAME/" $NETWORK_FILE
hostnamectl set-hostname $NEW_HOSTNAME --static
# Set DOMAIN
sed -i "s/^DOMAIN.*$/DOMAIN=$NEW_DOMAIN/" $ETH0_FILE
# Set IPADDR
sed -i "s/^IPADDR.*$/IPADDR=$NEW_IPADDR/" $ETH0_FILE
# Set NETMASK
sed -i "s/^NETMASK.*$/NETMASK=$NEW_NETMASK/" $ETH0_FILE
# Set GATEWAY
sed -i "s/^GATEWAY.*$/GATEWAY=$NEW_GATEWAY/" $ETH0_FILE
# Set DNS1
sed -i "s/^DNS1.*$/DNS1=$NEW_DNS1/" $ETH0_FILE
# Set DNS2
sed -i "s/^DNS2.*$/DNS2=$NEW_DNS2/" $ETH0_FILE
# Set BOOTPROTO
sed -i "s/^BOOTPROTO.*$/BOOTPROTO=none/" $ETH0_FILE
# Append Hosts File
echo "## Appended by $0"
echo "$NEW_IPADDR    $NEW_HOSTNAME    $NEW_HOSTNAME.$NEW_DOMAIN" >> $HOSTS_FILE
# Remove SSH Keys
rm -f /etc/ssh/ssh_host_*
echo " "
echo " "
cat $NETWORK_FILE
echo " "
cat $ETH0_FILE
echo " "
cat $HOSTS_FILE
echo " "

##############################################################################
# Cleanup, Disable, Reboot
#
#chmod a-x $0
#mv $0 "$0.done"
echo " "
echo "DONE: Changes written to disk."
echo "      Remember to change the root password and connect to puppet after reboot."
echo " "
read -p "Press [ENTER] to reboot now..."
reboot
