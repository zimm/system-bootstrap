#!/bin/bash

REPO_RPM='http://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm'

sudo yum localinstall -y $REPO_RPM
sudo yum install -y puppet-agent
echo 'export PATH=/opt/puppetlabs/bin:$PATH' | sudo tee --append /etc/profile.d/puppetlabs.sh
source /etc/profile.d/puppetlabs.sh
echo '172.25.90.72    puppet' | sudo tee --append /etc/hosts
sudo /opt/puppetlabs/bin/puppet agent -t

echo 'Agent installed and certificate signing requested.'
echo 'Please contact your puppet master to sign your certificate.'
echo 'It is a good idea to snapshot your virtual machine now - or after the first puppet run after signing.'
echo 'Also set noop=true in your /etc/puppetlabs/puppet/puppet.conf file to preview changes.'
echo 'Once certificate is signed run puppet agent -t again.'
