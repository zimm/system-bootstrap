# System Bootstrap

Scripts to help configure a minimum amount of functionality to connect to a puppet master.

Generally this repository will be cloned into /root/system-bootstrap on your virtual machine template.

## Script Inventory

- bootstrap-network.sh
- bootstrap-puppet_server.sh
- bootstrap-puppet_agent.sh
- bootstrap-network.ps1
- bootstrap-puppet_agent.ps1

## Compatability
The scripts in this repo are generally targeting CentOS 7, but may work with other RHEL based systems.
Other operating systems could be accounted for, but this repo is updated based on my current needs.

To keep things simple and portable
 - Linux based scripts should be written in BASH and leverage commonly installed utilities in a minimal install.
 - Windows based scripts (when added) should be written in Powershell.
 -- Batch file language could be used supplementary to invoke Powershell.

If something can't be minimally implemented in either BASH or Powershell it's a good indicator that's something we should be doing in our Puppet Control repository after we bootstrap the system.
