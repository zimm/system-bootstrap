#!/bin/bash

REPO_RPM='http://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm'

sudo yum localinstall -y $REPO_RPM
sudo yum install -y puppetserver
echo 'export PATH=/opt/puppetlabs/bin:$PATH' | sudo tee --append /etc/profile.d/puppet.sh
source /etc/profile.d/puppet.sh
sudo /opt/puppetlabs/bin/puppet cert generate `hostname -a` --dns_alt_names=puppet,puppet.trimet.org
sudo /opt/puppetlabs/bin/puppet module install zack-r10k
sudo mkdir -p /etc/puppetlabs/r10k
sudo /opt/puppetlabs/bin/puppet apply --modulepath=/etc/puppetlabs/code/environments/production/modules ./bootstrap-puppet.pp
sudo r10k deploy environment -pv
sudo service puppetserver start
sudo /opt/puppetlabs/bin/puppet agent -t
